#ifndef _SOC_CODA7542_H_
#define _SOC_CODA7542_H_

#include <linux/uio_coda7542.h>

#define UIO_CODA7542_NAME		"pxa-coda7542"

extern void __init pxa_register_coda7542(void);

#endif

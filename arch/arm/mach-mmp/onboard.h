#ifndef ONBOARD_H_
#define ONBOARD_H_

void __init abilene_add_lcd_mipi(void);
void __init yellowstone_add_lcd_mipi(void);
void __init orchid_add_lcd_mipi(void);
void __init mk2_add_lcd_mipi(void);
void __init emeidkb_add_lcd_mipi(void);
void __init emeidkb_add_lcd_mipi_tv(void);
void __init thunderstonem_add_lcd_mipi(void);
void __init mmp3_add_tv_out(void);
void __init dkb_add_lcd_tpo(void);
void __init dkb_add_lcd_truly(void);
void __init dkb_add_lcd_sharp(void);
void __init brownstone_add_lcd_mipi(void);
void __init mmp2_add_tv_out(void);
void __init emeidkb_add_lcd_pl(void);
void __init emeidkb_add_tv_out(void);

#endif /* ONBOARD_H_ */

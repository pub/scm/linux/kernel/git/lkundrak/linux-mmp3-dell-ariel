/*
 * PXA IPM IDLE
 *
 * Copyright (c) 2003 Intel Corporation.
 *
 * This software program is licensed subject to the GNU General Public License
 * (GPL).Version 2,June 1991, available at http://www.fsf.org/copyleft/gpl.html
 *
 * (C) Copyright 2006 Marvell International Ltd.
 * All Rights Reserved
 */

/*
#undef DEBUG
#define DEBUG
*/
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/init.h>
#include <linux/sched.h>
#include <linux/string.h>
#include <linux/errno.h>
#include <linux/proc_fs.h>
#include <linux/seq_file.h>
#include <linux/spinlock.h>
#include <linux/io.h>
#include <mach/hardware.h>
#include <asm/proc-fns.h>
#include <asm/mach/time.h>
#include <mach/pxa3xx-regs.h>
#include <mach/regs-intc.h>
#include <mach/regs-ost.h>
#include <mach/pxa95x_dvfm.h>
#include <mach/mspm_prof.h>
#ifdef CONFIG_ISPT
#include <mach/pxa_ispt.h>
#endif
#include <mach/pxa95x_pm.h>
#ifdef CONFIG_PXA_MIPSRAM
#include <mach/pxa_mips_ram.h>
#endif
#ifdef CONFIG_PXA95x_DVFM
#include <mach/dvfm.h>
#endif
#include <mach/pxa9xx_pm_logger.h> /* for pm debug tracing */
#include <mach/debug_pm.h>

#define MAX_OSCR0		0xFFFFFFFF

/* Low power mode idle interval is 2ms */
#define LOWPOWER_IDLE_INTERVAL  2

#define IDLE_STATS_NUM  1000

struct idle_stats {
	unsigned int index;
	unsigned int msec;
	unsigned int hr_data;	/* OSCR0, used in D0 */
	unsigned int lr_data;	/* OSCR4, used in D0, D1 or D2 */
	unsigned int icip;
	unsigned int icip2;
};

struct mspm_idle_stats {
	struct idle_stats stats[IDLE_STATS_NUM];
	unsigned int stats_index;
	spinlock_t lock;
};

static struct mspm_idle_stats mspm_stats = {
	.stats_index = 0,
	.lock = __SPIN_LOCK_UNLOCKED(mspm_stats.lock),
};

struct mspm_idle_prof {
	unsigned int start_oscr;
	unsigned int end_oscr;
	unsigned int oscr_idle_tick;
	unsigned int start_oscr4;
	unsigned int end_oscr4;
	unsigned int oscr4_idle_tick;
	unsigned int op;
	unsigned int jiffies;
};

static struct mspm_idle_prof prev_prof, cur_prof;

static int idle_flaw;		/* silicon issue on IDLE */

static void (*orig_idle) (void);
static unsigned int cpuid;
static int d1idx = -1, d2idx = -1, cgidx = -1, d2_suspend_idx = -1, cg_suspend_idx = -1;
extern int enable_deepidle;
#ifdef CONFIG_ISPT
#define ispt_power_state_c1() ispt_power_msg(CT_P_PWR_STATE_ENTRY_C1);
#define ispt_power_state_c0() ispt_power_msg(CT_P_PWR_STATE_ENTRY_C0);
#else
static int ispt_power_state_c1(void)
{
	return 0;
}

static int ispt_power_state_c0(void)
{
	return 0;
}
#endif

extern int is_wkr_mg1_1274(void);
/*This parameter is used in order to analyze C2 crash.
This parameter measure C2 length see JIRA 1495
This parameter will be extraced through RAMPDUMP*/
unsigned int g_lastC2time;

#ifdef CONFIG_CPU_PJ4
extern unsigned int pm_enter_deepidle(unsigned int);
#else
static unsigned int pm_enter_deepidle(unsigned int x)
{
	pr_err("%s should not be called in non-pj4 core code.\n", __func__);
	BUG_ON(1);
}
#endif
extern unsigned int c2_allow;
extern unsigned int c1_allow;

static inline unsigned int irq_base(int i)
{
	static unsigned int phys_base[] = {
		0x40d00000,
		0x40d0009c,
		0x40d00130,
	};
	return phys_base[i];
}

static void int_mask_no_uart(unsigned long *saved_icmr)
{
	int i;
	for (i = 0; i < 3; i++) {
		unsigned int addr_tmp = irq_base(i);
		void __iomem *base = ioremap(addr_tmp, 4);
		saved_icmr[i] = __raw_readl(base + (0x004));
		if (i == 0)
			__raw_writel(0x00700000, base + (0x004));
		else
			__raw_writel(0, base + (0x004));
		iounmap(base);
	}
}

static void int_mask_restore(unsigned long *saved_icmr)
{
	int i;
	for (i = 0; i < 3; i++) {
		unsigned int addr_tmp = irq_base(i);
		void __iomem *base = ioremap(addr_tmp, 4);
		__raw_writel(saved_icmr[i], base + (0x004));
		iounmap(base);
	}
}

static void pxa95x_cpu_idle(void)
{
	unsigned int c1_enter_time, c1_exit_time, pollreg;
	struct op_info *info = NULL;
	int op;
	unsigned long saved_icmr[4];
	DVFMLPMGlobalCount.D0C1_Enter_count++;
	op = dvfm_get_op(&info);

#ifdef CONFIG_PXA95x_DVFM_STATS
	if (op < 0) {
		printk(KERN_ERR "Fatal: current OP is not in OP table!\n");
		return;
	}
	dvfm_add_event(op, CPU_STATE_RUN, op, CPU_STATE_IDLE);
	dvfm_add_timeslot(op, CPU_STATE_RUN);
#endif
	mspm_add_event(op, CPU_STATE_RUN);

	ispt_power_state_c1();
#ifdef CONFIG_PXA_MIPSRAM
	c1_enter_time = OSCR4;
	MIPS_RAM_ADD_32K_TIME_STAMP(c1_enter_time);
	MIPS_RAM_ADD_PM_TRACE(ENTER_IDLE_MIPS_RAM);
	mipsram_disable_counter();
#endif
	if (cpu_is_pxa978()) {
		if (c2_allow) {
			PWRMODE = 0x0;
			PWRMODE = (PXA978_PM_S0D0CG | PXA95x_PM_I_Q_BIT);
			do {
				pollreg = PWRMODE;
			} while (pollreg !=
					(PXA978_PM_S0D0CG | PXA95x_PM_I_Q_BIT));
			if (ForceLPM == PXA9xx_Force_C2) {
				LastForceLPM = PXA9xx_Force_C2;
				int_mask_no_uart(saved_icmr);
			}

			pxa978_pm_enter(pollreg, NULL);

			if (ForceLPM == PXA9xx_Force_C2)
				int_mask_restore(saved_icmr);
		} else if (c1_allow) {
			PWRMODE = (PXA95x_PM_S0D0C1 | PXA95x_PM_I_Q_BIT);
			do {
				pollreg = PWRMODE;
			} while (pollreg != (PXA95x_PM_S0D0C1 | PXA95x_PM_I_Q_BIT));

			if (ForceLPM == PXA9xx_Force_C1) {
				LastForceLPM = PXA9xx_Force_C1;
				int_mask_no_uart(saved_icmr);
			}

			cpu_do_idle();

			if (ForceLPM == PXA9xx_Force_C1)
				int_mask_restore(saved_icmr);
		} else {
			if (ForceLPM == PXA9xx_Force_CORE_ONLY_IDLE) {
				LastForceLPM = PXA9xx_Force_CORE_ONLY_IDLE;
				int_mask_no_uart(saved_icmr);
			}

			cpu_do_idle();

			if (ForceLPM == PXA9xx_Force_CORE_ONLY_IDLE)
				int_mask_restore(saved_icmr);
		}
		if (!RepeatMode) {
			if (ForceLPM && LastForceLPM == ForceLPM)
				LastForceLPM = ForceLPM = PXA9xx_Force_None;
		}
	} else if (cpu_is_pxa955() && !(is_wkr_mg1_1274())) {
		PWRMODE = (PXA95x_PM_S0D0C1 | PXA95x_PM_I_Q_BIT);
		do {
			pollreg = PWRMODE;
		} while (pollreg != (PXA95x_PM_S0D0C1 | PXA95x_PM_I_Q_BIT));
		g_lastC2time = pm_enter_deepidle(CPU_PDWN_3_25M_CYCLES);
	} else {
		cpu_do_idle();
	}
#ifdef CONFIG_PXA_MIPSRAM
	mipsram_reinit_counter();
	MIPS_RAM_ADD_PM_TRACE(EXIT_IDLE_MIPS_RAM);
	c1_exit_time = OSCR4;
	MIPS_RAM_ADD_32K_TIME_STAMP(c1_exit_time);
#endif
	ispt_power_state_c0();

#ifdef CONFIG_PXA95x_DVFM_STATS
	dvfm_add_event(op, CPU_STATE_IDLE, op, CPU_STATE_RUN);
	dvfm_add_timeslot(op, CPU_STATE_IDLE);
#endif
	mspm_add_event(op, CPU_STATE_IDLE);
}

static int lpidle_is_valid(int enable, struct dvfm_freqs *freqs,
			   int lp_idle)
{
	struct op_info *info = NULL;
	struct dvfm_md_opt *op;
	int prev_op;
	int ret;

	if ((freqs == NULL) || (lp_idle == IDLE_D2 && d2idx == -1)
	    || (lp_idle == IDLE_D1 && d1idx == -1)
	    || (lp_idle == IDLE_CG && cgidx == -1))
		return 0;

	if (enable & lp_idle) {
		/* Check whether the specified low power mode is valid */
		ret = dvfm_get_op(&info);
		if (info == NULL)
			return 0;
		op = (struct dvfm_md_opt *)info->op;
		if (op == NULL)
			return 0;
		if ((ret >= 0) && (op->power_mode == POWER_MODE_D0)) {
			prev_op = ret;
			freqs->old = ret;

			freqs->new = lp_idle == IDLE_D2 ? d2idx :
			    lp_idle == IDLE_D1 ? d1idx :
			    lp_idle == IDLE_CG ? cgidx : 0xFFFFFFFF;
			if (freqs->new == 0xFFFFFFFF)
				return 0;
			ret = dvfm_get_opinfo(freqs->new, &info);
			if ((ret >= 0) && (info->device == 0))
				return 1;
		}
	}
	return 0;
}

/* Collect statistic information before entering idle */
static void record_idle_stats(unsigned int msec)
{
	int i;

	spin_lock(&mspm_stats.lock);
	if (++mspm_stats.stats_index == IDLE_STATS_NUM)
		mspm_stats.stats_index = 0;
	i = mspm_stats.stats_index;
	memset(&mspm_stats.stats[i], 0, sizeof(struct idle_stats));
	mspm_stats.stats[i].msec = msec;
	mspm_stats.stats[i].index = i;
	/* Record current OSCR */
	mspm_stats.stats[i].hr_data = OSCR;
	mspm_stats.stats[i].lr_data = OSCR4;
	spin_unlock(&mspm_stats.lock);
}

/* Collect statistic information after exiting idle.
 * Return OSCR interval
 */
static int update_idle_stats(void)
{
	unsigned int oscr, tmp;
	struct op_info *info = NULL;
	int i, op;

	op = dvfm_get_op(&info);
	spin_lock(&mspm_stats.lock);
	i = mspm_stats.stats_index;

	if (op != cur_prof.op) {
		/* If it's a new OP, refresh cur_prof */
		memcpy(&prev_prof, &cur_prof, sizeof(struct mspm_idle_prof));
		memset(&cur_prof, 0, sizeof(struct mspm_idle_prof));
		cur_prof.op = op;
		cur_prof.start_oscr = mspm_stats.stats[i].hr_data;
		cur_prof.start_oscr4 = mspm_stats.stats[i].lr_data;
		cur_prof.jiffies = jiffies;
	}

	tmp = mspm_stats.stats[i].hr_data;
	oscr = OSCR;
	if (oscr > tmp)
		tmp = oscr - tmp;
	else
		tmp = MAX_OSCR0 - oscr + tmp;
	/* update the OSCR0 interval into hr_data field */
	mspm_stats.stats[i].hr_data = tmp;

	tmp = mspm_stats.stats[i].lr_data;
	oscr = OSCR4;
	if (oscr > tmp)
		tmp = oscr - tmp;
	else
		tmp = MAX_OSCR0 - oscr + tmp;
	/* update the OSCR4 interval into lr_data field */
	mspm_stats.stats[i].lr_data = tmp;

	mspm_stats.stats[i].icip = ICIP;
	mspm_stats.stats[i].icip2 = ICIP2;

	cur_prof.oscr_idle_tick += mspm_stats.stats[i].hr_data;
	cur_prof.oscr4_idle_tick += mspm_stats.stats[i].lr_data;
	cur_prof.end_oscr = OSCR;
	cur_prof.end_oscr4 = OSCR4;

	spin_unlock(&mspm_stats.lock);

	return mspm_stats.stats[i].lr_data;
}

static unsigned int pxa_ticks_to_msec(unsigned int ticks)
{
#ifdef CONFIG_PXA_32KTIMER
	return (ticks * 5 * 5 * 5) >> 12;
#else
	return ticks / 3250;
#endif
}

/*
 * IDLE Thread
 * This function must be called with irq disabled
 */
void mspm_do_idle(void)
{
	struct dvfm_freqs freqs;
	int ret = -EINVAL;
	unsigned int msec, ticks;
	int delta;

	if (ForceC0) {
		local_irq_enable();
		return;
	}

	BUG_ON(!irqs_disabled());

#ifdef CONFIG_NO_IDLE_HZ
	timer_dyn_reprogram();
#endif
#ifdef CONFIG_PXA_32KTIMER
	ticks = (OSMR4 > OSCR4) ? OSMR4 - OSCR4
		: (0xFFFFFFFF - OSCR4 + OSMR4);
#else
	ticks = (OSMR0 > OSCR) ? OSMR0 - OSCR
		: (0xFFFFFFFF - OSCR + OSMR0);
#endif
	msec = pxa_ticks_to_msec(ticks);
	record_idle_stats(msec);

	/* Check idle interval */
	if (msec < LOWPOWER_IDLE_INTERVAL) {
		pm_logger_app_add_trace(1, PM_LPM_ABT_APPS, OSCR4, msec);
		pxa95x_cpu_idle();
		goto out;
	}

	if (lpidle_is_valid(enable_deepidle, &freqs, IDLE_D2)) {
		delta = dvfm_is_comm_wakep_near();
		/* checking if comm wakeup is to close and forbid D2
		 * if so.
		 */
		if (delta) {
			/* going to c1 instead... */
			pm_logger_app_add_trace(0, PM_LPM_ABT_COMM, OSCR4);
			pxa95x_cpu_idle();
			goto out;
		} else {
			/* this means that we are far
			 * enough and can enter D2.
			 * start D2 entry sequence.
			 */
			ret = dvfm_set_op(&freqs, freqs.new,
					RELATION_STICK);
			if (ret == 0)
				goto out;
		}
	}

	if (lpidle_is_valid(enable_deepidle, &freqs, IDLE_D1)) {
		delta = dvfm_is_comm_wakep_near();
		/* checking whether comm wakeup is too close
		 * and forbid D1 if so.
		 */
		if (delta) {
			/* going to c1 instead... */
			pm_logger_app_add_trace(0, PM_LPM_ABT_COMM, OSCR4);
			pxa95x_cpu_idle();
			goto out;
		} else {
			/* this means that we are far
			 * enough and can enter D2.
			 * start D1 entry sequence.
			 */
			ret = dvfm_set_op(&freqs, freqs.new,
					RELATION_STICK);
			if (ret == 0)
				goto out;
		}
	}

	if (lpidle_is_valid(enable_deepidle, &freqs, IDLE_CG)) {
		ret = dvfm_set_op(&freqs, freqs.new, RELATION_STICK);
		if (ret == 0)
			goto out;
	}

	pxa95x_cpu_idle();
out:
	update_idle_stats();
	local_irq_enable();
}

static struct proc_dir_entry *entry_dir;
static struct proc_dir_entry *entry_stats;

static int stats_show(struct seq_file *s, void *v)
{
	struct idle_stats *p = NULL;
	int i, ui;
	unsigned long flags;

	spin_lock_irqsave(&mspm_stats.lock, flags);

	ui = mspm_stats.stats_index;
	for (i = 0; i < IDLE_STATS_NUM; i++) {
		p = &mspm_stats.stats[ui++];
		seq_printf(s, "INDEX:%d MSECS:%u usec:%u "
			   "OSCR4DATA:%u ICIP:0x%x ICIP2:0x%x\n", p->index,
			   p->msec, p->hr_data * 4 / 13, p->lr_data,
			   p->icip, p->icip2);
		switch (p->icip) {
		case 0x4000000:
			seq_printf(s, ",OST0\n");
			break;
		case 0x400000:
			seq_printf(s, ",UART1\n");
			break;
		case 0x40000:
			seq_printf(s, ",I2C\n");
			break;
		case 0x400:
			seq_printf(s, ",GPIO_x\n");
			break;
		case 0x80:
			seq_printf(s, ",OST4\n");
			break;
		}
		if (ui == IDLE_STATS_NUM)
			ui = 0;
	}

	spin_unlock_irqrestore(&mspm_stats.lock, flags);

	seq_printf(s, "ICMR:0x%x, ICMR:0x%x\n", ICMR, ICMR2);
	/* set interrupt mask */
	return 0;
}

static int stats_seq_open(struct inode *inode, struct file *file)
{
	return single_open(file, &stats_show, NULL);
}

static const struct file_operations stats_seq_ops = {
	.owner = THIS_MODULE,
	.open = stats_seq_open,
	.read = seq_read,
	.llseek = seq_lseek,
	.release = single_release,
};

static int mspm_proc_init(void)
{
	entry_dir = proc_mkdir("driver/mspm", NULL);
	if (entry_dir == NULL)
		return -ENOMEM;

	entry_stats = create_proc_entry("stats", 0, entry_dir);
	if (entry_stats)
		entry_stats->proc_fops = &stats_seq_ops;
	return 0;
}

static void mspm_proc_cleanup(void)
{
	remove_proc_entry("stats", entry_dir);
	remove_proc_entry("driver/mspm", NULL);
}

int mspm_idle_load(void)
{
	orig_idle = pm_idle;
	pm_idle = mspm_do_idle;
	return 0;
}

void mspm_idle_clean(void)
{
	if (orig_idle != NULL)
		pm_idle = orig_idle;
	else
		/* in case mspm_idle_clean is called before mspm_idle_load */
		orig_idle = pm_idle;
}

static void query_idle_flaw(void)
{
	idle_flaw = 0;
	if ((cpuid >= 0x6880) && (cpuid <= 0x6881))
		idle_flaw = 1;	/* PXA300 A0/A1 */
	else if ((cpuid >= 0x6890) && (cpuid <= 0x6892))
		idle_flaw = 1;	/* PXA310 A0/A1/A2 */
}

void set_lowpower_op(int idx, int mode)
{
	switch (mode) {
	case POWER_MODE_D1:
		d1idx = idx;
		pr_info("d1idx : %d\n", d1idx);
		break;
	case POWER_MODE_D2:
		d2idx = idx;
		pr_info("d2idx : %d\n", d2idx);
		break;
	case POWER_MODE_CG:
		cgidx = idx;
		pr_info("cgidx : %d\n", cgidx);
		break;
	case POWER_MODE_D2_SUSPEND:
		d2_suspend_idx = idx;
		pr_info("d2_suspend_idx : %d\n", d2_suspend_idx);
		break;
	case POWER_MODE_CG_SUSPEND:
		cg_suspend_idx = idx;
		pr_info("cg_suspend_idx : %d\n", cg_suspend_idx);
		break;
	}
}

int get_lowpower_op(int mode)
{
	int ret = 0;
	switch (mode) {
	case POWER_MODE_D1:
		ret = d1idx;
		break;
	case POWER_MODE_D2:
		ret = d2idx;
		break;
	case POWER_MODE_CG:
		ret = cgidx;
		break;
	case POWER_MODE_D2_SUSPEND:
		ret = d2_suspend_idx;
		break;
	case POWER_MODE_CG_SUSPEND:
		ret = cg_suspend_idx;
		break;
	}
	if (ret)
		return ret;
	else
		return -EINVAL;
}

static int __init mspm_init(void)
{
	/* check idle flaw */
	cpuid = read_cpuid(0) & 0xFFFF;
	query_idle_flaw();

	/* Create file in procfs */
	if (mspm_proc_init()) {
		mspm_idle_clean();
		return -EFAULT;
	}

	/* clear data in prev_prof & cur_prof */
	memset(&prev_prof, 0, sizeof(struct mspm_idle_prof));
	memset(&cur_prof, 0, sizeof(struct mspm_idle_prof));
	prev_prof.op = -1;
	cur_prof.op = -1;

	pr_info("Initialize IPM.\n");

	return 0;
}

static void __exit mspm_exit(void)
{
	/* Remove procfs */
	mspm_proc_cleanup();

	pr_info("Quit IPM\n");
}

module_init(mspm_init);
module_exit(mspm_exit);

MODULE_DESCRIPTION("IPM");
MODULE_LICENSE("GPL");

/*
 * PXA3xx IPM Profiler
 *
 * Copyright (C) 2008 Marvell Corporation
 * Haojian Zhuang <haojian.zhuang@marvell.com>
 *
 * This software program is licensed subject to the GNU General Public License
 * (GPL).Version 2,June 1991, available at http://www.fsf.org/copyleft/gpl.html

 * (C) Copyright 2008 Marvell International Ltd.
 * All Rights Reserved
 */

/*
 * Behavior of profiler
 *
 * When profiler is enabled, the frequency can be adjusted by two reason.
 * 1. When sample window is finished, profiler calculates the mips in sample
 * window. System will be adjusted to suitable OP.
 *
 * 2. When sample window isn't finished and system enters low power mode
 * (D2/CG), sample window is reset. At this time, system may stays in high OP
 * . As complementation, profiler start a workqueue and request
 * system stay in lowest running operating point. Because entering low power
 * mode from high OP costs more power.
 *    System entering low power can be consider as very low mips cost.
 *    If profiler is disabled, the workqueue must be flushed. Then system
 * will stay in highest OP (624MHz).
 *    If system is busy, profiler will calculate new mips in sample window.
 * And system will switch to suitable OP from this OP.
 *
 * So when profiler is disabled, system always stays in 624MHz. System can
 * enters D2 idle, but the frequency won't be changed.
 */
#include <linux/init.h>
#include <linux/module.h>
#include <linux/errno.h>
#include <linux/sched.h>
#include <linux/tick.h>
#include <linux/timer.h>
#include <linux/device.h>
#include <linux/jiffies.h>
#include <linux/workqueue.h>

#include <mach/hardware.h>
#include <mach/mspm_prof.h>
#ifdef CONFIG_PXA95x_DVFM
#include <mach/dvfm.h>
#include <mach/pxa95x_dvfm.h>
#endif
#include <mach/regs-ost.h>
#include <mach/debug_pm.h>

/* idle watchdog timer */
#define IDLE_WATCHDOG_MSECS		40

struct mspm_op_stats {
	int op;
	int idle;
	unsigned int timestamp;
	unsigned int jiffies;
};

struct mspm_mips {
	int mips;
	int mem;
	int xsi;
	int h_thres;		/* high threshold */
	int l_thres;		/* low threshold */
};

extern struct kobject *power_kobj;

extern int mspm_idle_load(void);
extern void mspm_idle_clean(void);

/*
 * Store OP's MIPS in op_mips[].
 * The lowest frequency OP is the first entry.
 */
static struct mspm_mips op_mips[MAX_OP_NUM];

/* Store duration time of all OP in op_duration[]. */
static int op_duration[MAX_OP_NUM];

/* Store costed time in run_op_time[] & idle_op_time[] */
static unsigned int run_op_time[MAX_OP_NUM], idle_op_time[MAX_OP_NUM];

/*
 * Store the first timestamp of sample window in first_stats
 * Store the current timestamp of sample window in cur_stats
 */
static struct mspm_op_stats first_stats, cur_stats;

/* OP numbers used in IPM IDLE Profiler */
static int mspm_op_num;

static struct delayed_work idle_prof_work, idle_watchdog_work;
DEFINE_SPINLOCK(timer_lock);

static int mspm_prof_enabled;
static int window_jif, watchdog_jif;
static int restore_op = 0x5a;	/* magic number */

static int mspm_prof_notifier_freq(struct notifier_block *nb,
				   unsigned long val, void *data);
static struct notifier_block notifier_freq_block = {
	.notifier_call = mspm_prof_notifier_freq,
};

/* This workqueue is used to down frequency if system is entering D2 */
static struct work_struct down_freq;

extern int cur_op;

static int mspm_ctrl;
static int mspm_prof_ctrl;
static int mspm_mem_thres = 40;
static int mspm_window = DEF_SAMPLE_WINDOW;
/* Stores the flag of idle watchdog */
static int mspm_watchdog = 1;
/* Stores the miliseconds count of idle watchdog */
static int mspm_wtimer;
static int dvfm_dev_idx;
static struct ipm_profiler_arg pmu_arg;
static unsigned int read_time(void)
{
#ifdef CONFIG_PXA_32KTIMER
	return OSCR4;
#else
	return OSCR0;
#endif
}

/*
 * Adjust to the most appropriate OP according to MIPS result of
 * sample window
 */
int mspm_request_tune(int mips)
{
	int i;

	for (i = mspm_op_num - 1; i >= 0; i--) {
		if (mips >= (op_mips[i].l_thres * op_mips[i].mips / 100))
			break;
	}
	if (profilerRecommendationEnable)
		dvfm_request_op(profilerRecommendationPP);
	else
		dvfm_request_op(i);
	return 0;
}

static int idle_wd_valid;
static int check_idle_wd(int mips)
{
	int mips_thres = 200, max_idle_cnt = 3;
	static int idle_cnt;
	/* if mspm_watchdog isn't enabled, don't calculate */
	if (!mspm_watchdog)
		return 0;
	if (mips < mips_thres) {
		if (idle_cnt < max_idle_cnt) {
			idle_cnt++;
			idle_wd_valid = 1;
		} else
			idle_wd_valid = 0;
	} else {
		/* mips is larger */
		idle_wd_valid = 1;
		idle_cnt = 0;
	}
	return 0;
}

static int is_idle_wd_valid(void)
{
	if (mspm_watchdog)
		return idle_wd_valid;
	return 0;
}

/*
 * Calculate the MIPS in sample window
 */
static int mspm_calc_mips(unsigned int first_time)
{
	int i, mips;
	unsigned int time, sum_time = 0, sum = 0;

	/* Store the last slot as RUN state */
	time = read_time();
	run_op_time[cur_stats.op] += time - cur_stats.timestamp;
	cur_stats.timestamp = time;
	cur_stats.jiffies = jiffies;
	cur_stats.op = cur_op;
	cur_stats.idle = CPU_STATE_RUN;
	/* Calculate total time costed in sample window */
	for (i = 0; i < mspm_op_num; i++) {
		sum_time += run_op_time[i] + idle_op_time[i];
		sum += run_op_time[i] * op_mips[i].mips;
		op_duration[i] = run_op_time[i] + idle_op_time[i];
	}
	if (sum_time == 0) {
		/* CPU usage is 100% in current operating point */
		sum_time = time - first_time;
		sum = sum_time * op_mips[cur_op].mips;
		op_duration[cur_op] = sum_time;
	}

	/*
	 * Calculate MIPS in sample window
	 * Formula: run_op_time[i] / sum_time * op_mips[i].mips
	 */
	mips = sum / sum_time;
	check_idle_wd(mips);
	return mspm_request_tune(mips);
}

/*
 * Record the OP index and RUN/IDLE state.
 */
int mspm_add_event(int op, int cpu_idle)
{
	unsigned int time;

	if (mspm_prof_enabled) {
		if (is_idle_wd_valid()) {
			spin_lock(&timer_lock);
			if (timer_pending(&idle_watchdog_work.timer))
				mod_timer(&idle_watchdog_work.timer, jiffies + watchdog_jif);
			else
				schedule_delayed_work_on(0, &idle_watchdog_work, watchdog_jif);
			spin_unlock(&timer_lock);

		}
		time = read_time();
		/* sum the current sample window */
		if (cpu_idle == CPU_STATE_IDLE)
			idle_op_time[cur_stats.op] +=
			    time - cur_stats.timestamp;
		else if (cpu_idle == CPU_STATE_RUN)
			run_op_time[cur_stats.op] += time - cur_stats.timestamp;
		/* update start point of current sample window */
		cur_stats.op = op;
		cur_stats.idle = cpu_idle;
		cur_stats.timestamp = time;
		cur_stats.jiffies = jiffies;
	}
	return 0;
}
EXPORT_SYMBOL(mspm_add_event);

/*
 * Prepare to do a new sample.
 * Clear the index in mspm_op_stats table.
 */
static int mspm_do_new_sample(void)
{
	/* clear previous sample window */
	memset(&run_op_time, 0, sizeof(int) * MAX_OP_NUM);
	memset(&idle_op_time, 0, sizeof(int) * MAX_OP_NUM);
	/* prepare for the new sample window */
	first_stats.op = cur_stats.op;
	first_stats.idle = cur_stats.idle;
	first_stats.timestamp = read_time();
	first_stats.jiffies = jiffies;
	return 0;
}

/***************************************************************************
 *			Idle Profiler
 ***************************************************************************
 */

static int mspm_start_prof(struct ipm_profiler_arg *arg)
{
	struct op_info *info = NULL;

	/* pmu_arg.window_size stores the number of miliseconds.
	 * window_jif stores the number of jiffies.
	 */
	memset(&pmu_arg, 0, sizeof(struct ipm_profiler_arg));
	pmu_arg.flags = arg->flags;
	if (pmu_arg.window_size > 0)
		pmu_arg.window_size = arg->window_size;
	else
		pmu_arg.window_size = mspm_window;
	window_jif = msecs_to_jiffies(pmu_arg.window_size);
	watchdog_jif = msecs_to_jiffies(IDLE_WATCHDOG_MSECS);
	mspm_wtimer = IDLE_WATCHDOG_MSECS;

	/* start next sample window */
	cur_stats.op = dvfm_get_op(&info);
	cur_stats.idle = CPU_STATE_RUN;
	cur_stats.timestamp = read_time();
	cur_stats.jiffies = jiffies;
	mspm_do_new_sample();

	if (timer_pending(&idle_prof_work.timer))
		mod_timer(&idle_prof_work.timer, jiffies + window_jif);
	else
		schedule_delayed_work_on(0, &idle_prof_work, window_jif);
	mspm_prof_enabled = 1;
	/* start idle watchdog */
	if (is_idle_wd_valid()) {
		spin_lock(&timer_lock);
		if (timer_pending(&idle_watchdog_work.timer))
			mod_timer(&idle_watchdog_work.timer,
					jiffies + watchdog_jif);
		else
			schedule_delayed_work_on(0, &idle_watchdog_work,
					watchdog_jif);
		spin_unlock(&timer_lock);
	}

	return 0;
}

static int mspm_stop_prof(void)
{
	cancel_delayed_work_sync(&idle_prof_work);
	mspm_prof_enabled = 0;
	if (is_idle_wd_valid())
		cancel_delayed_work_sync(&idle_watchdog_work);
	/* flush workqueue */
	flush_scheduled_work();
	/* try to use 624MHz if it's not magic number */
	if (restore_op != 0x5a)
		dvfm_request_op(restore_op);
	return 0;
}

static unsigned int down_freq_op;
static void down_freq_worker(struct work_struct *work)
{
	/* request lowest running operating point */
	dvfm_request_op(down_freq_op);
}

/*
 * Handler of IDLE PROFILER
 */
static void idle_prof_handler(struct work_struct *work)
{
	mspm_calc_mips(first_stats.timestamp);

	/* start next sample window */
	mspm_do_new_sample();
	schedule_delayed_work_on(0, &idle_prof_work, window_jif);
	if (is_idle_wd_valid()) {
		spin_lock(&timer_lock);
		if (timer_pending(&idle_watchdog_work.timer))
			mod_timer(&idle_watchdog_work.timer, jiffies + watchdog_jif);
		else
			schedule_delayed_work_on(0, &idle_watchdog_work, watchdog_jif);
		spin_unlock(&timer_lock);
	}
}

static void idle_watchdog_handler(struct work_struct *work)
{
	if (mspm_prof_enabled) {
#ifdef CONFIG_PXA95x_DVFM
		struct op_info *info = NULL;
		struct dvfm_md_opt *md_op = NULL;
		int mips;
		dvfm_get_op(&info);
		md_op = (struct dvfm_md_opt *)info->op;
		mips = md_op->core;
		/* Now CPU usage is 100% in current operating point */
		mspm_request_tune(mips);
#endif
	}
}

extern void tick_nohz_update_jiffies(void);
/*
 * Pause idle profiler when system enter Low Power mode.
 * Continue it when system exit from Low Power mode.
 */
static int mspm_prof_notifier_freq(struct notifier_block *nb,
				   unsigned long val, void *data)
{
	struct dvfm_freqs *freqs = (struct dvfm_freqs *)data;
	struct op_info *info = &(freqs->new_info);
	struct dvfm_md_opt *md = NULL;
	int cpu;
	static int watchdog_timer_pending;

	if (!mspm_prof_enabled)
		return 0;
	/* only delete timer and stop tick for idle thread */
	cpu = smp_processor_id();
	if (!idle_cpu(cpu))
		return 0;
	md = (struct dvfm_md_opt *)(info->op);
	if (md->power_mode == POWER_MODE_D1 ||
	    md->power_mode == POWER_MODE_D2 ||
	    md->power_mode == POWER_MODE_CG) {
		switch (val) {
		case DVFM_FREQ_PRECHANGE:
			/* Cancel idle profiler timer without cancel workqueue.
			 * This is safe because this code is in idle thread.
			 * No workqueue is pending. */
			del_timer(&idle_prof_work.timer);
			if (is_idle_wd_valid() && timer_pending(
						&idle_watchdog_work.timer)) {
				del_timer(&idle_watchdog_work.timer);
				watchdog_timer_pending = 1;
			}
			tick_nohz_stop_sched_tick(1);
			break;
		case DVFM_FREQ_POSTCHANGE:
			/* Update jiffies and touch watchdog process */
			tick_nohz_update_jiffies();
#if 0
			/*
			 * If system always stays in 624MHz, check jiffies.
			 * If jiffies reaches sample window end,
			 * request new operating point according to calculation.
			 */
			if (jiffies > (first_stats.jiffies + window_jif))
				mspm_calc_mips(first_stats.timestamp);
#endif
			/*
			 * Restart the idle profiler because it's only
			 * disabled before entering low power mode.
			 * If we just continue the sample window with
			 * left jiffies, too much OS Timer wakeup exist
			 * in system.
			 * Just restart the sample window.
			 */
			mod_timer(&idle_prof_work.timer, jiffies + window_jif);
			if (is_idle_wd_valid()) {
				spin_lock(&timer_lock);
				if (watchdog_timer_pending) {
					mod_timer(&idle_watchdog_work.timer,
							jiffies + watchdog_jif);
					watchdog_timer_pending = 0;
				} else
					schedule_delayed_work_on(0,
							&idle_watchdog_work,
							watchdog_jif);
				spin_unlock(&timer_lock);
			}
			first_stats.jiffies = jiffies;
			first_stats.timestamp = read_time();
			/* Try to down the frequency to at least 156MHz */
			schedule_work(&down_freq);
			break;
		}
	}
	return 0;
}

/* It's invoked by initialization code & sysfs interface */
static int launch_mspm(void)
{
	struct ipm_profiler_arg arg;
	if (mspm_ctrl && MSPM_PROFILER != cur_profiler) {
		pr_info("mspm profiler is not supported.\n");
		mspm_ctrl = mspm_prof_ctrl = 0;
		return 0;
	}
	if (mspm_ctrl) {
		mspm_idle_load();
		/* check whether profiler should be launched */
		if (mspm_prof_ctrl) {
			memset(&pmu_arg, 0, sizeof(struct ipm_profiler_arg));
			arg.flags = IPM_IDLE_PROFILER /* | IPM_PMU_PROFILER */ ;
			arg.window_size = mspm_window;
			mspm_start_prof(&arg);
		}
	} else {
		/* disable profiler */
		if (mspm_prof_ctrl)
			mspm_stop_prof();
		mspm_idle_clean();
	}
	return 0;
}

/************************************************************************
 *			sysfs interface					*
 ************************************************************************
 */

#define mspm_attr(_name)				\
static struct kobj_attribute _name##_attr = {		\
	.attr	= {					\
		.name = __stringify(_name),		\
		.mode = 0644,				\
	},						\
	.show	= _name##_show,				\
	.store	= _name##_store,			\
}

/*
 * Show whether MSPM is enabled
 */
static ssize_t mspm_show(struct kobject *kobj, struct kobj_attribute *attr,
			 char *buf)
{
	return sprintf(buf, "%u\n", mspm_ctrl);
}

/*
 * Configure MSPM
 * When MSPM is enabled, mspm idle is loaded.
 */
static ssize_t mspm_store(struct kobject *kobj, struct kobj_attribute *attr,
			  const char *buf, size_t len)
{
	sscanf(buf, "%u", &mspm_ctrl);
	launch_mspm();
	return len;
}

mspm_attr(mspm);

int mspm_state(void)
{
	return mspm_ctrl;
}
EXPORT_SYMBOL(mspm_state);

void mspm_disable(void)
{
	mspm_ctrl = 0;
	launch_mspm();
}
EXPORT_SYMBOL(mspm_disable);

void mspm_enable(void)
{
	mspm_ctrl = 1;
	launch_mspm();
}
EXPORT_SYMBOL(mspm_enable);

/*
 * Show whether MSPM profiler in kerenl space is enabled
 */
static ssize_t prof_show(struct kobject *kobj, struct kobj_attribute *attr,
			 char *buf)
{
	return sprintf(buf, "%u\n", mspm_prof_ctrl);
}

/*
 * Configure to MSPM profiler in kernel space
 * This interface can't control deepidle
 */
static ssize_t prof_store(struct kobject *kobj,
			  struct kobj_attribute *attr, const char *buf,
			  size_t len)
{
	struct ipm_profiler_arg arg;
	int prof_enabled;
	prof_enabled = mspm_prof_ctrl;
	sscanf(buf, "%u", &mspm_prof_ctrl);
	if (mspm_prof_ctrl && MSPM_PROFILER != cur_profiler) {
		pr_info("mspm profiler is not supported.\n");
		mspm_ctrl = mspm_prof_ctrl = 0;
		goto out;
	}
	if (!mspm_ctrl)
		goto out;
	/* check whether profiler should be launched */
	if (mspm_prof_ctrl) {
		memset(&pmu_arg, 0, sizeof(struct ipm_profiler_arg));
		arg.flags = IPM_IDLE_PROFILER /* | IPM_PMU_PROFILER */ ;
		arg.window_size = mspm_window;
		mspm_start_prof(&arg);
	} else {
		/* disable profiler */
		if (prof_enabled)
			mspm_stop_prof();
	}
out:
	return len;
}

mspm_attr(prof);

/*
 * stall interface is used to set the memory stall parameter
 */
static ssize_t mem_thres_show(struct kobject *kobj,
			      struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%uMB/s\n", mspm_mem_thres);
}

static ssize_t mem_thres_store(struct kobject *kobj,
			       struct kobj_attribute *attr,
			       const char *buf, size_t len)
{
	sscanf(buf, "%u", &mspm_mem_thres);
	return len;
}

mspm_attr(mem_thres);

/* Show the length of sample window */
static ssize_t window_show(struct kobject *kobj,
			   struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%ums\n", mspm_window);
}

static ssize_t window_store(struct kobject *kobj,
			    struct kobj_attribute *attr, const char *buf,
			    size_t len)
{
	sscanf(buf, "%u", &mspm_window);
	return len;
}

mspm_attr(window);

static ssize_t watchdog_show(struct kobject *kobj,
			     struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%u\n", mspm_watchdog);
}

static ssize_t watchdog_store(struct kobject *kobj,
			      struct kobj_attribute *attr, const char *buf,
			      size_t len)
{
	sscanf(buf, "%u", &mspm_watchdog);
	if (mspm_watchdog) {
		spin_lock(&timer_lock);
		if (timer_pending(&idle_watchdog_work.timer))
			mod_timer(&idle_watchdog_work.timer, jiffies + watchdog_jif);
		else
			schedule_delayed_work_on(0, &idle_watchdog_work, watchdog_jif);
		spin_unlock(&timer_lock);
	} else
		cancel_delayed_work_sync(&idle_watchdog_work);
	return len;
}

mspm_attr(watchdog);

static ssize_t wtimer_show(struct kobject *kobj,
			   struct kobj_attribute *attr, char *buf)
{
	return sprintf(buf, "%ums\n", mspm_wtimer);
}

static ssize_t wtimer_store(struct kobject *kobj,
			    struct kobj_attribute *attr, const char *buf,
			    size_t len)
{
	sscanf(buf, "%u", &mspm_wtimer);
	if (mspm_wtimer > 0)
		watchdog_jif = msecs_to_jiffies(mspm_wtimer);
	return len;
}

mspm_attr(wtimer);

static struct attribute *g[] = {
	&mspm_attr.attr,
	&prof_attr.attr,
	&mem_thres_attr.attr,
	&window_attr.attr,
	&watchdog_attr.attr,
	&wtimer_attr.attr,
	NULL,
};

static struct attribute_group attr_group = {
	.name = "mspm",
	.attrs = g,
};

/*
 * Init MIPS of all OP
 * Return OP numbers
 */
int __init mspm_init_mips(void)
{
	int i, low_pp_found = 0;
	memset(&op_mips, 0, MAX_OP_NUM * sizeof(struct mspm_mips));
#ifdef CONFIG_PXA95x_DVFM
	mspm_op_num = dvfm_op_count();
	for (i = 0; i < mspm_op_num; i++) {
		struct op_info *info = NULL;
		struct dvfm_md_opt *md_op = NULL;
		int ret;
		ret = dvfm_get_opinfo(i, &info);
		if (ret)
			continue;
		md_op = (struct dvfm_md_opt *) info->op;

		if (md_op->core == 156 && !low_pp_found) {
			down_freq_op = i;
			low_pp_found = 1;
		}
		op_mips[i].mips = md_op->core;
		op_mips[i].mem = md_op->dmcfs;
		if (op_mips[i].mips) {
			op_mips[i].h_thres = DEF_HIGH_THRESHOLD;
			op_mips[i].xsi = 13 * md_op->xl;
			if (!strcmp(md_op->name, "624M"))
				restore_op = i;
		} else {
			/* Low Power mode won't be considered */
			mspm_op_num = i;
			break;
		}
	}
	for (i = 0; i < mspm_op_num - 1; i++)
		op_mips[i + 1].l_thres = op_mips[i].h_thres * op_mips[i].mips
		    / op_mips[i + 1].mips;
#endif
	if (!low_pp_found)
		printk(KERN_ERR "%s Low PP was not found for IdleProfiler, \
		 down_freq_op =%d\n", __func__, down_freq_op);
	return mspm_op_num;
}

int __init mspm_prof_init(void)
{
	if (sysfs_create_group(power_kobj, &attr_group))
		return -EFAULT;

	memset(&pmu_arg, 0, sizeof(struct ipm_profiler_arg));
	pmu_arg.window_size = mspm_window;
	window_jif = msecs_to_jiffies(pmu_arg.window_size);

	/* It's used to trigger sample window.
	 * If system is idle, the timer could be deferred.
	 */
	INIT_DELAYED_WORK_DEFERRABLE(&idle_prof_work, idle_prof_handler);

	/* It's used to monitor IDLE event */
	INIT_DELAYED_WORK_DEFERRABLE(&idle_watchdog_work, idle_watchdog_handler);

	mspm_op_num = mspm_init_mips();

	if (cur_profiler == CPUFREQ_PROFILER)
		mspm_watchdog = 0;

	dvfm_register_notifier(&notifier_freq_block, DVFM_FREQUENCY_NOTIFIER);
	dvfm_register("MSPM PROF", &dvfm_dev_idx);

	INIT_WORK(&down_freq, down_freq_worker);
	return 0;
}

void __exit mspm_prof_exit(void)
{
	dvfm_unregister("MSPM PROF", &dvfm_dev_idx);
	dvfm_unregister_notifier(&notifier_freq_block, DVFM_FREQUENCY_NOTIFIER);
}

module_init(mspm_prof_init);
module_exit(mspm_prof_exit);

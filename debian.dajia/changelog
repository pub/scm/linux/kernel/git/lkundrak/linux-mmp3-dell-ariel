linux-dajia (3.0.31-1.21) precise; urgency=low

  [Jesse Sung]

  * enable netconsole support
  * enable bluetooth support

 -- Jesse Sung <jesse.sung@canonical.com>  Thu, 02 May 2013 23:51:36 +0800

linux-dajia (3.0.31-1.20) precise; urgency=low

  [Jesse Sung]

  * Fix kernel panic when open mmp3_sysset
    - LP: #1171356

 -- Jesse Sung <jesse.sung@canonical.com>  Wed, 24 Apr 2013 13:37:49 +0800

linux-dajia (3.0.31-1.19) precise; urgency=low

  [Jesse Sung]

  * enable usb audio support
    - LP: #1167382

 -- Jesse Sung <jesse.sung@canonical.com>  Thu, 11 Apr 2013 13:55:31 +0800

linux-dajia (3.0.31-1.18) precise; urgency=low

  [Jesse Sung]

  * merge marvell's 20130404 3.0a-rc2 changes
    - LP: #1166684
  * Use Ariel instead of Qseven as hardware name

 -- Jesse Sung <jesse.sung@canonical.com>  Wed, 10 Apr 2013 14:54:56 +0800

linux-dajia (3.0.31-1.17) precise; urgency=low

  [Jesse Sung]

  * merge marvell's 3.0a-rc1 changes
    - LP: #1162714
  * CONFIG: make DRM and VIVANTE builtin
  * merge SPI flash and EC SPI scan code support from Paul
    - LP: #1162715
  * CONFIG: enable CONFIG_SPI_SPIDEV
  * CONFIG: set CONFIG_DEFAULT_MMAP_MIN_ADDR to 4096 instead
  * CONFIG: change default cpufreq gov to performance
    - LP: #1162669
  * CONFIG: enable CONFIG_USB_VIDEO_CLASS
    - LP: #1163213

 -- Jesse Sung <jesse.sung@canonical.com>  Tue, 02 Apr 2013 16:28:31 +0800

linux-dajia (3.0.31-0.16) precise; urgency=low

  [Jesse Sung]

  * CONFIG: Enable DEBUG_NOTIFIERS

 -- Jesse Sung <jesse.sung@canonical.com>  Thu, 14 Mar 2013 20:06:23 +0800

linux-dajia (3.0.31-0.15) precise; urgency=low

  [Jesse Sung]

  * CONFIG: Enable TRACER
  * Apply ureadahead patches

 -- Jesse Sung <jesse.sung@canonical.com>  Thu, 14 Mar 2013 17:28:24 +0800

linux-dajia (3.0.31-0.14) precise; urgency=low

  [Jesse Sung]

  * Disable CONFIG_HAVE_HW_BREAKPOINT

 -- Jesse Sung <jesse.sung@canonical.com>  Thu, 14 Mar 2013 14:31:07 +0800

linux-dajia (3.0.31-0.13) precise; urgency=low

  [Jesse Sung]

  * Enable some options related to profiling

 -- Jesse Sung <jesse.sung@canonical.com>  Wed, 13 Mar 2013 11:04:42 +0800

linux-dajia (3.0.31-0.12) precise; urgency=low

  [Jesse Sung]

  * Merge 2.0b-rc2 kernel source drop
    - fix EC version read
    - LP: #1151137
  * When random MAC addressis ued , do not change the address on each
    interface up/down
    - LP: #1152330

 -- Jesse Sung <jesse.sung@canonical.com>  Fri, 08 Mar 2013 17:07:56 +0800

linux-dajia (3.0.31-0.11) precise; urgency=low

  [Jesse Sung]

  * Disable FRAMEBUFFER_CONSOLE
    - LP: #1136479

 -- Jesse Sung <jesse.sung@canonical.com>  Thu, 07 Mar 2013 17:35:47 +0800

linux-dajia (3.0.31-0.10) precise; urgency=low

  [Jesse Sung]

  * Merge Marvell 2.0b kernel source drop
    - LP: #1146223
  * Review kernel config
    - LP: #1143997
  * Change default bootargs in kernel
    - LP: #1146046

 -- Jesse Sung <jesse.sung@canonical.com>  Tue, 05 Mar 2013 17:56:06 +0800

linux-dajia (3.0.31-0.9) precise; urgency=low

  [Jesse Sung]

  * Import AUFS and make it a module
    - LP: #1135600

  [Rex Tsai]

  * Remove SRCPKGNAME-headers-PKGVER-ABINUM dependency
    - LP: #1135743

 -- Jesse Sung <jesse.sung@canonical.com>  Thu, 28 Feb 2013 22:26:49 +0800

linux-dajia (3.0.31-0.8) precise; urgency=low

  * enable support for Winbond W25X SPI flash (LP: #1134566)
  * disable rbswap (LP: #1130977)

 -- Jesse Sung <jesse.sung@canonical.com>  Thu, 28 Feb 2013 16:59:58 +0800

linux-dajia (3.0.31-0.7) precise; urgency=low

  * enable SquashFS (LP: #1133218)

 -- Jesse Sung <jesse.sung@canonical.com>  Tue, 26 Feb 2013 18:42:05 +0800

linux-dajia (3.0.31-0.6) precise; urgency=low

  * enable CONFIG_DM_SNAPSHOT
  * disable CONFIG_DM_MULTIPATH and CONFIG_DM_DELAY

 -- Jesse Sung <jesse.sung@canonical.com>  Fri, 22 Feb 2013 16:02:07 +0800

linux-dajia (3.0.31-0.5) precise; urgency=low

  * remove crda from dependency list (LP: #1131130)
  * enable initrd kernel options (LP: #1131122)
  * fix display color setting (LP: #1130977)
  * cherry-picked ethernet driver updates (LP: #1130466)

 -- Jesse Sung <jesse.sung@canonical.com>  Thu, 21 Feb 2013 20:55:43 +0800

linux-dajia (3.0.31-0.4) precise; urgency=low

  [ Jesse Sung ]

  * Update kernel options
    + enable DEVTMPFS
    + enable DEVPTS_MULTIPLE_INSTANCES and LEGACY_PTYS
    + enable BTRFS
    + make DRM and VIVANTE modules

 -- Jesse Sung <jesse.sung@canonical.com>  Tue, 19 Feb 2013 21:51:10 +0800

linux-dajia (3.0.31-0.3) precise; urgency=low

  [ Jesse Sung ]

  * disable CPU hotplug_timer (LP: 1119658)

 -- Jesse Sung <jesse.sung@canonical.com>  Tue, 19 Feb 2013 12:42:30 +0800

linux-dajia (3.0.31-0.2) precise; urgency=low

  [ Jesse Sung ]

  * Alpha4_dual_display/Ariel2_Dual_Display.patch applied

 -- Jesse Sung <jesse.sung@canonical.com>  Fri, 08 Feb 2013 16:44:52 +0800

linux-dajia (3.0.31-0.1) precise; urgency=low

  [ Jesse Sung ]

  * bump kernel version
  * make CONFIG_DRM and CONFIG_DRM_VIVANTE built-in
  * enable CONFIG_HID_A4TECH and CONFIG_HID_LOGITECH as module
  * make CONFIG_HID_NTRIG as a module instead

 -- Jesse Sung <jesse.sung@canonical.com>  Fri, 08 Feb 2013 15:06:52 +0800

linux-dajia (3.0.1-0.1) precise; urgency=low

  [ Jesse Sung ]

  * Initial import

 -- Jesse Sung <jesse.sung@canonical.com>  Mon, 04 Feb 2013 18:00:53 +0800

/*
 * 88pm867-regulator.c
 * Marvell Buck regulators driver
 *
 * Copyright 2012 Marvell Semiconductor, Inc.
 * Author: Dipen Patel
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 */
#include <linux/err.h>
#include <linux/string.h>
#include <linux/kernel.h>
#include <linux/module.h>
#include <linux/param.h>
#include <linux/init.h>
#include <linux/slab.h>
#include <linux/i2c.h>
#include <linux/platform_device.h>
#include <linux/regulator/88pm867.h>
#include <linux/regulator/driver.h>
#include <linux/regulator/machine.h>
#include <linux/idr.h>

struct mar88pm867_vreg {
	struct regulator_dev *rdev;
	struct regulator_desc desc;
	struct regulator_init_data *regulator;
	struct mutex io_lock;
	struct i2c_client *client;
	u8 id;
	u8 volt_reg;
	u8 volt_shadow;
	u8 cfg_shadow;
	u8 enbl_shadow;
	u8 cfg_reg;
	u8 enbl_reg;
	int enable_bit;
	u32 min_uV;
	u32 max_uV;
	u32 step_uV;
	u8 power_mode;
};

static DEFINE_IDR(regulator_id);
static DEFINE_MUTEX(regulator_mutex);

static int mar88pm867_regulator_set_voltage(struct regulator_dev *rdev,
					    int min_uV, int max_uV,
					    unsigned *selector);
static int mar88pm867_regulator_get_voltage(struct regulator_dev *dev);
static int mar88pm867_regulator_enable(struct regulator_dev *dev);
static int mar88pm867_regulator_disable(struct regulator_dev *dev);
static int mar88pm867_regulator_is_enabled(struct regulator_dev *dev);
static int mar88pm867_regulator_set_mode(struct regulator_dev *dev,
					unsigned int power_mode);
static unsigned int mar88pm867_regulator_get_mode(struct regulator_dev *dev);

static struct regulator_ops mar88pm867_ops = {
	.set_voltage =  mar88pm867_regulator_set_voltage,
	.get_voltage = mar88pm867_regulator_get_voltage,
	.enable = mar88pm867_regulator_enable,
	.disable = mar88pm867_regulator_disable,
	.is_enabled = mar88pm867_regulator_is_enabled,
	.set_mode = mar88pm867_regulator_set_mode,
	.get_mode = mar88pm867_regulator_get_mode,
};

static int mar88pm867_i2c_write(struct i2c_client *i2c, unsigned char addr,
			       void *src, unsigned int bytes)
{
	unsigned char buf[bytes + 1];
	int ret;
	buf[0] = addr;
	memcpy(&buf[1], src, bytes);
	ret = i2c_master_send(i2c, buf, bytes + 1);
	if (ret < 0)
		return ret;
	return 0;
}

static int mar88pm867_i2c_read(struct i2c_client *i2c, unsigned char addr,
				unsigned char *dest, unsigned int bytes)
{
	int ret;
	if (bytes > 1) {
		ret =
		    i2c_smbus_read_i2c_block_data(i2c, addr, bytes, dest);
	} else {
		ret = i2c_smbus_read_byte_data(i2c, addr);
		if (ret < 0)
			return ret;
		*dest = (unsigned char) ret;
	}
	return 0;
}

int mar88pm867_write(struct mar88pm867_vreg *chip, u8 addr, u8 * values,
		     unsigned int len)
{
	int ret;
	if (chip == NULL)
		return -EINVAL;
	mutex_lock(&chip->io_lock);
	ret = mar88pm867_i2c_write(chip->client, addr, values, len);
	mutex_unlock(&chip->io_lock);
	return ret;
}
EXPORT_SYMBOL(mar88pm867_write);

int mar88pm867_read(struct mar88pm867_vreg *chip, u8 addr, u8 * values,
		    unsigned int len)
{
	int ret;
	if (chip == NULL)
		return -EINVAL;
	mutex_lock(&chip->io_lock);
	ret = mar88pm867_i2c_read(chip->client, addr, values, len);
	mutex_unlock(&chip->io_lock);
	return ret;
}
EXPORT_SYMBOL(mar88pm867_read);

static int mar88pm867_regulator_set_voltage(struct regulator_dev *rdev,
					    int min_uV, int max_uV,
					    unsigned *selector)
{
	struct mar88pm867_vreg *vreg = rdev_get_drvdata(rdev);
	u8 val;
	int rc = -EIO;

	if (min_uV < vreg->min_uV || max_uV > vreg->max_uV) {
		dev_err(&vreg->client->dev, \
			"invalid voltage range (%d, %d) uV\n", \
			min_uV, max_uV);
		return -EDOM;
	}

	val = 2 + ((min_uV - vreg->min_uV) / vreg->step_uV);
	dev_dbg(&vreg->client->dev, \
		"%s RequV:%d, MinuV:%d, StpuV:%d, val:0x%x\n", \
		__func__, min_uV, vreg->min_uV, vreg->step_uV, val);

	rc = mar88pm867_write(vreg, vreg->volt_reg, &val, 1);
	if (rc == 0)
		vreg->volt_shadow = val;

	return rc;
}

static int mar88pm867_regulator_get_voltage(struct regulator_dev *rdev)
{
	struct mar88pm867_vreg *vreg = rdev_get_drvdata(rdev);
	u8 val;
	int volt;
	int rc = -EIO;

	rc = mar88pm867_read(vreg, vreg->volt_reg, &val, 1);
	if (rc < 0)
		return rc;
	vreg->volt_shadow = val;
	volt = ((val - 0x2) * vreg->step_uV) + vreg->min_uV;
	dev_dbg(&vreg->client->dev, "%s: val:0x%x volt:%d\n", \
		__func__, val, volt);
	return volt;
}

static int mar88pm867_regulator_enable(struct regulator_dev *rdev)
{
	int rc = -EDOM;
	rc = mar88pm867_regulator_set_mode(rdev, MAR88PM867_MODE_NORMAL);
	return rc;
}

static int mar88pm867_regulator_disable(struct regulator_dev *rdev)
{
	int rc = -EDOM;
	rc = mar88pm867_regulator_set_mode(rdev, MAR88PM867_MODE_DISABLE);
	return rc;
}

static int mar88pm867_regulator_is_enabled(struct regulator_dev *rdev)
{
	return (mar88pm867_regulator_get_mode(rdev) == MAR88PM867_MODE_NORMAL);
}

static int mar88pm867_regulator_set_mode(struct regulator_dev *rdev,
					 unsigned int power_mode)
{
	struct mar88pm867_vreg *vreg = rdev_get_drvdata(rdev);
	int rc = -EIO;
	u8 value;
	if (power_mode == MAR88PM867_MODE_NORMAL ||
		power_mode == MAR88PM867_MODE_DISABLE) {
		value = vreg->enbl_shadow & ~(1 << vreg->enable_bit) |
			((u8)power_mode << vreg->enable_bit);
		rc = mar88pm867_write(vreg, vreg->enbl_reg, &value, 1);

		if (rc == 0)
			vreg->power_mode = power_mode;
		dev_dbg(&vreg->client->dev, "%s id:%d, mode:%d\n", __func__,
				vreg->id, vreg->power_mode);
	return rc;
	}
	/*FIXME for sleep mode which can be used as
	optimum mode in dead idle*/
	else
		return rc;
}

static unsigned int mar88pm867_regulator_get_mode(struct regulator_dev
						  *rdev)
{
	struct mar88pm867_vreg *vreg = rdev_get_drvdata(rdev);
	int rc = -EIO;
	u8 val = -1;
	rc = mar88pm867_read(vreg, vreg->enbl_reg, &val, 1);
	if (rc == 0) {
		vreg->enbl_shadow = val;
		vreg->power_mode =
			(vreg->enbl_shadow) >> vreg->enable_bit & 0x1;
	}
	if (rc < 0)
		return rc;
	dev_dbg(&vreg->client->dev, "%s id:%d, val:0x%x, mode:%d\n", __func__,
		  vreg->id, val, vreg->power_mode);

	return (unsigned int)vreg->power_mode;
}

static int mar88pm867_setup(struct mar88pm867_vreg *vreg)
{
	u8 data = 0x0;
	int rc = 0;

	rc = mar88pm867_read(vreg, vreg->volt_reg, &vreg->volt_shadow, 1);
	if (rc < 0)
		return rc;
	rc = mar88pm867_read(vreg, vreg->enbl_reg, &vreg->enbl_shadow, 1);
	if (rc < 0)
		return rc;
	rc = mar88pm867_read(vreg, vreg->cfg_reg, &vreg->cfg_shadow, 1);

	/*
	  * Set VCXO_EN pin to control sleep_n
	  */

	data = (0x1 << 2) | 0x50;
	rc = mar88pm867_write(vreg, MAR88PM867_SYS_CTRL_REG, &data, 1);
	if (rc < 0)
		return rc;

	data = 0x14; /*set buck1 target voltage 1.1 for sleep mode*/
	rc = mar88pm867_write(vreg, MAR88PM867_VREG_SD0_SLEEP, &data, 1);
	if (rc < 0)
		return rc;
	pr_info("Marvell 88pg867 setup is ready for regulator\n");
	return 0;
};

#if 1
static int voltage_show(struct device *dev, struct device_attribute *attr,
				char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct mar88pm867_vreg *di = i2c_get_clientdata(client);
	int ret;

	ret = mar88pm867_regulator_get_voltage(di->rdev);
	if (ret < 0) {
		dev_err(dev, "Can't get voltage!\n");
		return 0;
	}
	return sprintf(buf, "%d\n", ret);
}

static int voltage_store(struct device *dev, struct device_attribute *attr,
				const char *buf, size_t count)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct mar88pm867_vreg *di = i2c_get_clientdata(client);
	unsigned int vol, sel;
	int ret;

	ret = strict_strtoul(buf, 10, &vol);
	if (ret < 0)
		dev_err(dev, "Can't get voltage!\n");

	ret = mar88pm867_regulator_set_voltage(di->rdev, vol, vol, &sel);
	if (ret < 0)
		dev_err(dev, "Can't set voltage!\n");
	return count;
}

static DEVICE_ATTR(voltage, S_IRUGO | S_IWUSR, voltage_show, voltage_store);

static int regs_show(struct device *dev, struct device_attribute *attr,
				char *buf)
{
	struct i2c_client *client = to_i2c_client(dev);
	struct mar88pm867_vreg *vreg = i2c_get_clientdata(client);
	int ret = 0;
	u8 val = -1;
	int ret1 = 0;

	ret = mar88pm867_read(vreg, vreg->enbl_reg, &val, 1);
	if (ret < 0)
		dev_err(dev, "Can't get enable register value!\n");

	ret1 += sprintf(buf + ret1, "Enable register:   0x%x\n", (int)val);

	ret = mar88pm867_read(vreg, vreg->volt_reg, &val, 1);
	if (ret < 0)
		dev_err(dev, "Can't get normal target register value!\n");

	ret1 += sprintf(buf + ret1, "Normal target register: 0x%x\n", (int)val);

	ret = mar88pm867_read(vreg, vreg->cfg_reg, &val, 1);
	if (ret < 0)
		dev_err(dev, "Can't get config register value!\n");

	ret1 += sprintf(buf + ret1, "Config register:   0x%x\n", (int)val);

	ret = mar88pm867_read(vreg, MAR88PM867_SYS_CTRL_REG, &val, 1);
	if (ret < 0)
		dev_err(dev, "Can't get sys control register value!\n");

	ret1 += sprintf(buf + ret1, "sys control register:   0x%x\n", (int)val);

	ret = mar88pm867_read(vreg, MAR88PM867_SYS_STATUS_REG, &val, 1);
	if (ret < 0)
		dev_err(dev, "Can't get sys status register value!\n");

	ret1 += sprintf(buf + ret1, "sys status register:   0x%x\n", (int)val);

	return ret1;
}

static DEVICE_ATTR(regs, S_IRUGO | S_IWUSR, regs_show, NULL);

static struct attribute *mar88pm867_attributes[] = {
	&dev_attr_voltage.attr,
	&dev_attr_regs.attr,
	NULL,
};

static struct attribute_group mar88pm867_attr_grp = {
	.attrs = mar88pm867_attributes,
};

#endif

static int mar88pm867_regulator_register(struct mar88pm867_vreg *vreg,
		struct mar88pm867_platform_data *pdata, char *name, int num)
{
	int rc = 0;
	struct regulator_desc *rdesc = &vreg->desc;
	rdesc->name = name;
	rdesc->id = num;
	rdesc->ops = &mar88pm867_ops;
	rdesc->type = REGULATOR_VOLTAGE;
	rdesc->owner = THIS_MODULE;

	vreg->id = pdata->buck_id;
	vreg->min_uV = 775000;
	vreg->max_uV = 1400000;
	vreg->step_uV = 25000;
	vreg->volt_reg = MAR88PM867_VREG_SD0;
	vreg->cfg_reg = MAR88PM867_VREG_SD0_CFG;
	vreg->power_mode = MAR88PM867_MODE_NORMAL;
	vreg->enable_bit = 0;
	vreg->enbl_reg = MAR88PM867_ENABLE_REG;

	rc = mar88pm867_setup(vreg);
	if (rc < 0)
		return rc;

	vreg->rdev = regulator_register(&vreg->desc, &vreg->client->dev,
					vreg->regulator, vreg);
	if (IS_ERR(vreg->rdev))
		return PTR_ERR(vreg->rdev);
	return 0;

}

static const struct i2c_device_id mar88pm867_id[] = {
	    {"mar88pm867", -1},
		{},
};

static int mar88pm867_probe(struct i2c_client *client,
			    const struct i2c_device_id *id)
{
	struct mar88pm867_vreg *vreg = NULL;
	char *name;
	int num, ret = 0;

	struct mar88pm867_platform_data *pdata = client->dev.platform_data;

	if (!pdata || !pdata->regulator) {
		dev_err(&client->dev, "missing platform data\n");
		return -EINVAL;
	}
	vreg = kzalloc(sizeof(struct mar88pm867_vreg), GFP_KERNEL);
	if (vreg == NULL) {
		pr_err("%s: kzalloc() failed.\n", __func__);
		return -ENOMEM;
	}
	vreg->client = client;
	vreg->regulator = pdata->regulator;
	mutex_init(&vreg->io_lock);
	i2c_set_clientdata(client, vreg);

	/* Get a new ID for the new device */
	ret = idr_pre_get(&regulator_id, GFP_KERNEL);
	if (ret == 0) {
		ret = -ENOMEM;
		dev_err(&client->dev, "Can't get new id!\n");
		goto err_idr_pre_get;
	}
	mutex_lock(&regulator_mutex);
	ret = idr_get_new(&regulator_id, vreg, &num);
	mutex_unlock(&regulator_mutex);
	if (ret < 0) {
		dev_err(&client->dev, "Can't get new id!\n");
		goto err_idr_get_new;
	}
	/* Generate a name with new id */
	name = kasprintf(GFP_KERNEL, "%s-%d", id->name, num);

	/* regulator register */
	ret = mar88pm867_regulator_register(vreg, pdata, name, num);
	if (ret < 0) {
		dev_err(&client->dev, "Failed to register regulator!\n");
		goto err_rdev_register;
	}
	/* Create sysfs interface */
	ret = sysfs_create_group(&client->dev.kobj, &mar88pm867_attr_grp);
	if (ret) {
		dev_err(&client->dev, "Failed to create sysfs group!\n");
		goto err_create_sysfs;
	}
	dev_info(&client->dev, "Marvell 88pg867 Regulator is enabled!\n");

	return 0;
err_create_sysfs:
	regulator_unregister(vreg->rdev);
err_rdev_register:
	kfree(name);
	mutex_lock(&regulator_mutex);
	idr_remove(&regulator_id, num);
	mutex_unlock(&regulator_mutex);
err_idr_pre_get:
err_idr_get_new:
	kfree(vreg);
	return ret;

}

static int __devexit mar88pm867_remove(struct i2c_client *client)
{
	struct mar88pm867_vreg *di = i2c_get_clientdata(client);
	sysfs_remove_group(&client->dev.kobj, &mar88pm867_attr_grp);
	regulator_unregister(di->rdev);

	kfree(di->desc.name);
	mutex_lock(&regulator_mutex);
	idr_remove(&regulator_id, di->desc.id);
	mutex_unlock(&regulator_mutex);

	kfree(di);
	return 0;
}

static struct i2c_driver mar88pm867_driver = {
	.driver = {
		.name = "mar88pm867-regulator",
		.owner = THIS_MODULE,
	},
	.probe = mar88pm867_probe,
	.remove = __devexit_p(mar88pm867_remove),
	.id_table = mar88pm867_id,
};

static int __init mar88pm867_i2c_init(void)
{
	int rc;
	rc = i2c_add_driver(&mar88pm867_driver);
	if (rc != 0)
		pr_err("Failed to add mar88pm867 i2c driver: rc = %d\n",
			rc);
	else
		pr_info("%s: i2c add driver: rc = %d\n",
			 __func__, rc);
	return rc;
}
subsys_initcall(mar88pm867_i2c_init);

static void __exit mar88pm867_i2c_exit(void)
{
	i2c_del_driver(&mar88pm867_driver);
}
module_exit(mar88pm867_i2c_exit);

MODULE_AUTHOR("Dipen Patel <dpatel@marvell.com>");
MODULE_LICENSE("GPL v2");
MODULE_DESCRIPTION("mar88pm867 regulator driver");
MODULE_VERSION("1.0");

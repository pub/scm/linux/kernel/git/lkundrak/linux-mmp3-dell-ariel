/*
 * linux/sound/soc/pxa/mmp3asoc.c
 *
 * Copyright (C) 2009 Marvell International Ltd.
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#include <linux/module.h>
#include <linux/moduleparam.h>
#include <linux/timer.h>
#include <linux/interrupt.h>
#include <linux/platform_device.h>
#include <linux/fs.h>
#include <linux/proc_fs.h>
#include <sound/core.h>
#include <sound/pcm.h>
#include <sound/soc.h>
#include <sound/soc-dapm.h>
#include <linux/i2c.h>
#include <linux/regulator/machine.h>

#include <asm/mach-types.h>
#include <linux/io.h>
#include <linux/switch.h>

#include <linux/uaccess.h>
#include <plat/ssp.h>
#include <mach/addr-map.h>
#include <mach/regs-sspa.h>
#include <mach/regs-mpmu.h>
#include <mach/regs-apmu.h>

#include "../codecs/88pm805-codec.h"
#include "mmp2-squ.h"
#include "mmp2-sspa.h"
#include <linux/delay.h>

#define MMP3ASOC_SAMPLE_RATES SNDRV_PCM_RATE_44100

#define MMP3ASOC_HEADPHONE_FUNC		0
#define MMP3ASOC_HS_MIC_FUNC		1
#define MMP3ASOC_SPK_FUNC			2
#define MMP3ASOC_MAIN_MIC_FUNC		3

#define MMP3ASOC_CTRL_ON	0
#define MMP3ASOC_CTRL_OFF	1

static int mmp3asoc_headphone_func;
static int mmp3asoc_hs_mic_func;
static int mmp3asoc_spk_func;
static int mmp3asoc_main_mic_func;
static struct regulator *v_5v;

static void mmp3asoc_ext_control(struct snd_soc_dapm_context *dapm, int func)
{
	switch (func) {
	case MMP3ASOC_HEADPHONE_FUNC:
		if (mmp3asoc_headphone_func == MMP3ASOC_CTRL_ON)
			snd_soc_dapm_enable_pin(dapm, "Headset Stereophone");
		else
			snd_soc_dapm_disable_pin(dapm, "Headset Stereophone");
		break;
	case MMP3ASOC_HS_MIC_FUNC:
		if (mmp3asoc_hs_mic_func == MMP3ASOC_CTRL_ON)
			snd_soc_dapm_enable_pin(dapm, "Headset Mic 2");
		else
			snd_soc_dapm_disable_pin(dapm, "Headset Mic 2");
		break;
	case MMP3ASOC_SPK_FUNC:
		if (mmp3asoc_spk_func == MMP3ASOC_CTRL_ON) {
			snd_soc_dapm_enable_pin(dapm, "Ext Spk");
		} else {
			snd_soc_dapm_disable_pin(dapm, "Ext Spk");
		}
		break;
	case MMP3ASOC_MAIN_MIC_FUNC:
		if (mmp3asoc_main_mic_func == MMP3ASOC_CTRL_ON) {
			snd_soc_dapm_enable_pin(dapm, "Ext Mic 1");
			snd_soc_dapm_enable_pin(dapm, "Ext Mic 3");
		} else {
			snd_soc_dapm_disable_pin(dapm, "Ext Mic 1");
			snd_soc_dapm_disable_pin(dapm, "Ext Mic 3");
		}
		break;
	default:
		pr_err("wrong func type\n");
		return;
	}

	snd_soc_dapm_sync(dapm);
	return;
}

static int mmp3asoc_get_headphone(struct snd_kcontrol *kcontrol,
			       struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = mmp3asoc_headphone_func;
	return 0;
}

static int mmp3asoc_set_headphone(struct snd_kcontrol *kcontrol,
			       struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);
	struct snd_soc_dapm_context *dapm = &codec->dapm;

	if (mmp3asoc_headphone_func == ucontrol->value.integer.value[0])
		return 0;

	mutex_lock(&codec->mutex);
	mmp3asoc_headphone_func = ucontrol->value.integer.value[0];
	mmp3asoc_ext_control(dapm, MMP3ASOC_HEADPHONE_FUNC);
	mutex_unlock(&codec->mutex);
	return 1;
}

static int mmp3asoc_get_hs_mic(struct snd_kcontrol *kcontrol,
			       struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = mmp3asoc_hs_mic_func;
	return 0;
}

static int mmp3asoc_set_hs_mic(struct snd_kcontrol *kcontrol,
			       struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);
	struct snd_soc_dapm_context *dapm = &codec->dapm;

	if (mmp3asoc_hs_mic_func == ucontrol->value.integer.value[0])
		return 0;

	mutex_lock(&codec->mutex);
	mmp3asoc_hs_mic_func = ucontrol->value.integer.value[0];
	mmp3asoc_ext_control(dapm, MMP3ASOC_HS_MIC_FUNC);
	mutex_unlock(&codec->mutex);
	return 1;
}

static int mmp3asoc_get_spk(struct snd_kcontrol *kcontrol,
			      struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = mmp3asoc_spk_func;
	return 0;
}

static int mmp3asoc_set_spk(struct snd_kcontrol *kcontrol,
			      struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);
	struct snd_soc_dapm_context *dapm = &codec->dapm;

	if (mmp3asoc_spk_func == ucontrol->value.integer.value[0])
		return 0;

	mutex_lock(&codec->mutex);
	mmp3asoc_spk_func = ucontrol->value.integer.value[0];
	mmp3asoc_ext_control(dapm, MMP3ASOC_SPK_FUNC);
	mutex_unlock(&codec->mutex);
	return 1;
}

static int mmp3asoc_get_main_mic(struct snd_kcontrol *kcontrol,
			       struct snd_ctl_elem_value *ucontrol)
{
	ucontrol->value.integer.value[0] = mmp3asoc_main_mic_func;
	return 0;
}

static int mmp3asoc_set_main_mic(struct snd_kcontrol *kcontrol,
			       struct snd_ctl_elem_value *ucontrol)
{
	struct snd_soc_codec *codec = snd_kcontrol_chip(kcontrol);
	struct snd_soc_dapm_context *dapm = &codec->dapm;

	if (mmp3asoc_main_mic_func == ucontrol->value.integer.value[0])
		return 0;

	mutex_lock(&codec->mutex);
	mmp3asoc_main_mic_func = ucontrol->value.integer.value[0];
	mmp3asoc_ext_control(dapm, MMP3ASOC_MAIN_MIC_FUNC);
	mutex_unlock(&codec->mutex);
	return 1;
}

static const struct snd_soc_dapm_widget mmp3asoc_dapm_widgets[] = {
	SND_SOC_DAPM_HP("Headphone Stereophone", NULL),
	SND_SOC_DAPM_LINE("Lineout Out 1", NULL),
	SND_SOC_DAPM_LINE("Lineout Out 2", NULL),
	SND_SOC_DAPM_SPK("Ext Speaker", NULL),
	SND_SOC_DAPM_MIC("Ext Mic 1", NULL),
	SND_SOC_DAPM_MIC("Headset Mic", NULL),
	SND_SOC_DAPM_MIC("Ext Mic 3", NULL),
};

static const struct snd_soc_dapm_route mmp3asoc_dapm_routes[] = {
	{"Headset Stereophone", NULL, "HS1"},
	{"Headset Stereophone", NULL, "HS2"},

	{"Ext Speaker", NULL, "LSP"},
	{"Ext Speaker", NULL, "LSN"},

	{"Lineout Out 1", NULL, "LINEOUT1"},
	{"Lineout Out 2", NULL, "LINEOUT2"},

	{"MIC1P", NULL, "Mic1 Bias"},
	{"MIC1N", NULL, "Mic1 Bias"},
	{"Mic1 Bias", NULL, "Ext Mic 1"},

	{"MIC2P", NULL, "Mic1 Bias"},
	{"MIC2N", NULL, "Mic1 Bias"},
	{"Mic1 Bias", NULL, "Headset Mic 2"},

	{"MIC3P", NULL, "Mic3 Bias"},
	{"MIC3N", NULL, "Mic3 Bias"},
	{"Mic3 Bias", NULL, "Ext Mic 3"},
};

static const char *const jack_function[] = {
	"Headphone", "Mic", "Headset", "Off" };

static const char *headphone_function[] = {"On", "Off"};
static const char *hs_mic_function[] = {"On", "Off"};
static const char *spk_function[] = {"On", "Off"};
static const char *main_mic_function[] = {"On", "Off"};

static const struct soc_enum mmp3asoc_enum[] = {
	SOC_ENUM_SINGLE_EXT(2, headphone_function),
	SOC_ENUM_SINGLE_EXT(2, hs_mic_function),
	SOC_ENUM_SINGLE_EXT(2, spk_function),
	SOC_ENUM_SINGLE_EXT(2, main_mic_function),
};

static const struct snd_kcontrol_new mmp3asoc_elba_controls[] = {
	SOC_ENUM_EXT("Headphone Function", mmp3asoc_enum[0],
		     mmp3asoc_get_headphone, mmp3asoc_set_headphone),
	SOC_ENUM_EXT("Headset Mic Function", mmp3asoc_enum[1],
		     mmp3asoc_get_hs_mic, mmp3asoc_set_hs_mic),
	SOC_ENUM_EXT("Speaker Function", mmp3asoc_enum[2],
		     mmp3asoc_get_spk, mmp3asoc_set_spk),
	SOC_ENUM_EXT("Main Mic Function", mmp3asoc_enum[3],
		     mmp3asoc_get_main_mic, mmp3asoc_set_main_mic),
};

static int codec_hdmi_init(struct snd_soc_pcm_runtime *rtd)
{
	return 0;
}

static struct platform_device *mmp3asoc_snd_device[2];

#define __raw_modify(addr, toclear, toset)		\
	do {						\
		volatile unsigned int tval;		\
		tval = __raw_readl(addr);		\
		tval &= ~toclear;			\
		tval |= toset;				\
		__raw_writel(tval, (addr));		\
		tval = __raw_readl(addr);		\
		udelay(100);				\
	} while (0)

static void audio_subsystem_poweron(void)
{
	printk(KERN_INFO " audio subsystem power on (A stepping) 0x%08x\n",
	       __raw_readl(APMU_REG(0x220)));

	/* enable power switch 01 */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0, (1u << 9));
	/* enable power switch 11 */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0, (2u << 9));
	/* enable audio memory redundancy start */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0, (1u << 2));
	/* enable SRAM power */
	__raw_modify(APMU_AUDIO_SRAM_PWR, 0, 0x5);
	__raw_modify(APMU_AUDIO_SRAM_PWR, 0, 0xa);
	__raw_modify(APMU_AUDIO_SRAM_PWR, 0, 0xc0);
	/* audio island */
	__raw_modify(APMU_ISLD_DSPA_CTRL, 0x7, 0);
	__raw_modify(APMU_ISLD_DSPA_CTRL, 0, (1u << 4));
	__raw_modify(APMU_ISLD_DSPA_CTRL, (1u << 4), 0);
	/* audio DSA */
	__raw_modify(APMU_AUDIO_DSA, 0xf, 0xa);
	__raw_modify(APMU_AUDIO_DSA, 0xf, 0xf);
	/* SSPA1 BIT/SYSCLK */
	__raw_writel(0xd3ee2276, MPMU_ISCCRX0);
	__raw_writel(0xd0040040, MPMU_ISCCRX1);
	/* disable isolation */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0, (1u << 8));
	/* enable peripheral */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0, (1u << 4));
	/* pull peripheral out of reset */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0, (1u << 1));

	/* Audio CFG: DSP core will stall after release from reset */
	__raw_modify(DSP_AUDIO_CONFIG_REG, (1u << 1), 0);
	/* DSP core clock : enable clock divier  */
	__raw_modify(DSA_CORE_CLK_RES_CTRL, 0, (1u << 3));
	/* Release the core reset */
	__raw_modify(DSA_CORE_CLK_RES_CTRL, 0, (1u << 0));
	/* Release the AXI reset */
	__raw_modify(DSA_CORE_CLK_RES_CTRL, 0, (1u << 1));

	/* devices */
	udelay(100);
	__raw_writel(0x8, DSA_SSP_CLK_RES_CTRL);
	__raw_writel(0x9, DSA_SSP_CLK_RES_CTRL);
	udelay(100);
	__raw_writel(0x8, DSA_ABU_CLK_RES_CTRL);
	__raw_writel(0x9, DSA_ABU_CLK_RES_CTRL);
	udelay(100);

	__raw_modify(MPMU_CCGR, 0, 0x20);

	udelay(1000);
}

static void audio_subsystem_poweroff(void)
{
	/* enable isolation */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0x100, 0);
	/* assert AXI and peripheral reset */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0x3, 0);
	/* gate axi and peripheral clock */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0x18, 0);
	/* power off */
	__raw_modify(APMU_AUDIO_CLK_RES_CTRL, 0x600, 0);
}

static void audio_subsystem_pll_config(void)
{
	/* select audio pll: Fvco = 135.4752MHz; OCLK = 22.5792MHz;
	 * DIV_MODULO[2:0] = 0b001;
	 * FRACT[27:8] = 0x8a18;
	 * DIV_FBCCLK[1:0] = 0b00; */
	__raw_writel(0x908a1899, SSPA_AUD_PLL_CTRL0);
	msleep(100);

	/* sspa_aud_pll_ctrl1[11] = 1 to choose audio PLL
	 * div : 12, 44.1K
	 * DIV_OCLK_PATTERN[1:0] = 0b01; */
	__raw_writel(0x2e010801, SSPA_AUD_PLL_CTRL1);
	msleep(100);

	/* audio clock select: choose separate clock source for sspa1 and sspa2.
	 * sspa_aud_ctrl[7] = 0, sspa1 chooses pm_vctcxo;
	 * sspa_aud_ctrl[23] = 0, sspa2 chooses i2s_sc_apb; */
	__raw_writel(0x111109, SSPA_AUD_CTRL);
	msleep(100);

	/* Switch the source clock for core and AXI clock to Audio PLL*/
	__raw_modify(DSA_CORE_CLK_RES_CTRL, 0x0, (1u << 2));
	__raw_modify(DSA_CORE_CLK_RES_CTRL, 0x0, (3u << 4));
	__raw_modify(DSA_CORE_CLK_RES_CTRL, (1u << 2), 0x0);

}

static int codec_elba_init(struct snd_soc_pcm_runtime *rtd)
{
	struct snd_soc_codec *codec = rtd->codec;
	struct snd_soc_dapm_context *dapm = &codec->dapm;
	int err;

	audio_subsystem_poweron();
	/* currently the audio pll of mmp3 a0 stepping is not working */
	audio_subsystem_pll_config();

	/* Open elba speaker power for ThunderstoneM,
	   recently, other platform need not */
	if (machine_is_thunderstonem()) {
		v_5v = regulator_get(NULL, "V_5V");
		if (IS_ERR(v_5v)) {
			pr_err("%s fail to get regulator V_5V speaker\n",
					__func__);
			return -EINVAL;
		}

		regulator_enable(v_5v);
	}

#if 0
	/* Add mmp3asoc specific controls */
	err = snd_soc_add_controls(codec, mmp3asoc_elba_controls,
				   ARRAY_SIZE(mmp3asoc_elba_controls));
	if (err < 0)
		return err;

	/* add mmp3asoc specific widgets */
	snd_soc_dapm_new_controls(dapm, mmp3asoc_dapm_widgets,
				  ARRAY_SIZE(mmp3asoc_dapm_widgets));

	/* set up mmp3asoc specific audio routes */
	snd_soc_dapm_add_routes(dapm, mmp3asoc_dapm_routes,
				ARRAY_SIZE(mmp3asoc_dapm_routes));

	snd_soc_dapm_enable_pin(dapm, "Ext Speaker");
	snd_soc_dapm_enable_pin(dapm, "Ext Mic 1");
	snd_soc_dapm_enable_pin(dapm, "Ext Mic 3");
	snd_soc_dapm_disable_pin(dapm, "Headset Mic 2");
	snd_soc_dapm_disable_pin(dapm, "Headset Stereophone");

	/* set endpoints to not connected */
	snd_soc_dapm_nc_pin(dapm, "AUX1");
	snd_soc_dapm_nc_pin(dapm, "AUX2");
	snd_soc_dapm_nc_pin(dapm, "MIC1P");
	snd_soc_dapm_nc_pin(dapm, "MIC1N");
	snd_soc_dapm_nc_pin(dapm, "MIC2P");
	snd_soc_dapm_nc_pin(dapm, "MIC2N");
	snd_soc_dapm_nc_pin(dapm, "MIC3P");
	snd_soc_dapm_nc_pin(dapm, "MIC3N");

	/* output widget */
	snd_soc_dapm_nc_pin(dapm, "HS1");
	snd_soc_dapm_nc_pin(dapm, "HS2");
	snd_soc_dapm_nc_pin(dapm, "LINEOUT1");
	snd_soc_dapm_nc_pin(dapm, "LINEOUT2");
	snd_soc_dapm_nc_pin(dapm, "EARP");
	snd_soc_dapm_nc_pin(dapm, "EARN");
	snd_soc_dapm_nc_pin(dapm, "LSP");
	snd_soc_dapm_nc_pin(dapm, "LSN");

	mutex_lock(&codec->mutex);
	mmp3asoc_headphone_func = MMP3ASOC_CTRL_OFF;
	mmp3asoc_hs_mic_func = MMP3ASOC_CTRL_OFF;
	mmp3asoc_spk_func = MMP3ASOC_CTRL_OFF;
	mmp3asoc_main_mic_func = MMP3ASOC_CTRL_OFF;
	mmp3asoc_ext_control(dapm, MMP3ASOC_HEADPHONE_FUNC);
	mmp3asoc_ext_control(dapm, MMP3ASOC_HS_MIC_FUNC);
	mmp3asoc_ext_control(dapm, MMP3ASOC_SPK_FUNC);
	mmp3asoc_ext_control(dapm, MMP3ASOC_MAIN_MIC_FUNC);
	snd_soc_dapm_sync(dapm);
	mutex_unlock(&codec->mutex);
#endif
	return 0;
}

static int mmp3asoc_probe(struct snd_soc_card *card)
{
	return 0;
}

static int mmp3asoc_hdmi_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;

	cpu_dai->driver->playback.channels_min = 2;
	cpu_dai->driver->playback.channels_max = 2;

	cpu_dai->driver->playback.formats = SNDRV_PCM_FMTBIT_S16_LE;
	cpu_dai->driver->playback.rates = MMP3ASOC_SAMPLE_RATES;

	return 0;
}

static int mmp3asoc_hdmi_hw_params(struct snd_pcm_substream *substream,
			      struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	int freq_in, freq_out, sspa_mclk, sysclk, sspa_div;

	pr_debug("%s: enter, rate %d\n", __func__, params_rate(params));

	freq_in = 26000000;
	if (params_rate(params) > 11025) {
		freq_out = params_rate(params) * 512;
		sysclk = params_rate(params) * 256;
		sspa_mclk = params_rate(params) * 64;
	} else {
		freq_out = params_rate(params) * 1024;
		sysclk = params_rate(params) * 512;
		sspa_mclk = params_rate(params) * 64;
	}
	sspa_div = freq_out;
	do_div(sspa_div, sspa_mclk);

#ifdef CONFIG_SND_ELBA_MASTER_MODE
	snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S |
			    SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFM);
	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
			    SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFM);
#else
	snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S |
			    SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
			    SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
#endif

	/* workaround for audio PLL, and should be removed after A1 */
	switch (params_rate(params)) {
	case 48000:
		__raw_writel(0xd0040040, MPMU_ISCCRX1);
		break;
	case 44100:
		__raw_writel(0xd0040044, MPMU_ISCCRX1);
		break;
	case 32000:
		__raw_writel(0xd00800c0, MPMU_ISCCRX1);
		break;
	case 24000:
		__raw_writel(0xd0020040, MPMU_ISCCRX1);
		break;
	case 22050:
		__raw_writel(0xd0020044, MPMU_ISCCRX1);
		break;
	case 16000:
		__raw_writel(0xd00400c0, MPMU_ISCCRX1);
		break;
	case 8000:
		__raw_writel(0xd00400c0, MPMU_ISCCRX1);
		break;
	default:
		break;
	}

	/* SSPA clock ctrl register changes, and can't use previous API */
	snd_soc_dai_set_pll(cpu_dai, SSPA_AUDIO_PLL, 0, freq_in, freq_out);
	snd_soc_dai_set_clkdiv(cpu_dai, 0, sspa_div);
	snd_soc_dai_set_sysclk(cpu_dai, 0, sysclk, 0);

	return 0;
}
static int mmp3asoc_elba_startup(struct snd_pcm_substream *substream)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;

	cpu_dai->driver->playback.formats = SNDRV_PCM_FMTBIT_S16_LE;
	cpu_dai->driver->capture.formats = SNDRV_PCM_FMTBIT_S16_LE;
	cpu_dai->driver->playback.rates = MMP3ASOC_SAMPLE_RATES;
	cpu_dai->driver->capture.rates = MMP3ASOC_SAMPLE_RATES;

	return 0;
}

static int mmp3asoc_elba_hw_params(struct snd_pcm_substream *substream,
			      struct snd_pcm_hw_params *params)
{
	struct snd_soc_pcm_runtime *rtd = substream->private_data;
	struct snd_soc_dai *codec_dai = rtd->codec_dai;
	struct snd_soc_dai *cpu_dai = rtd->cpu_dai;
	int freq_in, freq_out, sspa_mclk, sysclk, sspa_div;

	pr_debug("%s: enter, rate %d\n", __func__, params_rate(params));

	freq_in = 26000000;
	if (params_rate(params) > 11025) {
		freq_out = params_rate(params) * 512;
		sysclk = params_rate(params) * 256;
		sspa_mclk = params_rate(params) * 64;
	} else {
		freq_out = params_rate(params) * 1024;
		sysclk = params_rate(params) * 512;
		sspa_mclk = params_rate(params) * 64;
	}
	sspa_div = freq_out;
	do_div(sspa_div, sspa_mclk);

#ifdef CONFIG_SND_ELBA_MASTER_MODE
	snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S |
			    SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFM);
	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
			    SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBM_CFM);
#else
	snd_soc_dai_set_fmt(codec_dai, SND_SOC_DAIFMT_I2S |
			    SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
	snd_soc_dai_set_fmt(cpu_dai, SND_SOC_DAIFMT_I2S |
			    SND_SOC_DAIFMT_NB_NF | SND_SOC_DAIFMT_CBS_CFS);
#endif

	/* workaround for audio PLL, and should be removed after A1 */
	/* SSPA2 clock formula: sysclk = (PLL1/4) * ISCCR1 Nom/Denom4
	 * for 48k, the sysclk should be 12.2880MHz, but here we only get
	 * approximate 12.458MHz */
	switch (params_rate(params)) {
	case 48000:
		__raw_writel(0xd0040040, MPMU_ISCCRX1);
		break;
	case 44100:
		__raw_writel(0xd0040044, MPMU_ISCCRX1);
		break;
	case 32000:
		__raw_writel(0xd00800c0, MPMU_ISCCRX1);
		break;
	case 24000:
		__raw_writel(0xd0020040, MPMU_ISCCRX1);
		break;
	case 22050:
		__raw_writel(0xd0020044, MPMU_ISCCRX1);
		break;
	case 16000:
		__raw_writel(0xd00400c0, MPMU_ISCCRX1);
		break;
	case 8000:
		__raw_writel(0xd00400c0, MPMU_ISCCRX1);
		break;
	default:
		break;
	}

	/* SSPA clock ctrl register changes, and can't use previous API */
	snd_soc_dai_set_pll(cpu_dai, SSPA_AUDIO_PLL, 0, freq_in, freq_out);
	snd_soc_dai_set_clkdiv(cpu_dai, 0, sspa_div);
	snd_soc_dai_set_sysclk(cpu_dai, 0, sysclk, 0);

	/* set elba sysclk */
	snd_soc_dai_set_sysclk(codec_dai, 0, 0, PM805_CODEC_CLK_DIR_OUT);

	return 0;
}

#ifdef CONFIG_PM
static int mmp3asoc_suspend_post(struct snd_soc_card *card)
{
	/* Control ThunderstoneM speaker power dynamicly */
	if (machine_is_thunderstonem()) {
		regulator_disable(v_5v);
	}

	return 0;
}

static int mmp3asoc_resume_pre(struct snd_soc_card *card)
{
	/* Control ThunderstoneM speaker power dynamicly */
	if (machine_is_thunderstonem()) {
		regulator_enable(v_5v);
	}

	return 0;
}

#endif

/* machine stream operations */
static struct snd_soc_ops mmp3asoc_machine_ops[] = {
	{
	 .startup = mmp3asoc_hdmi_startup,
	 .hw_params = mmp3asoc_hdmi_hw_params,
	 },
	{
	 .startup = mmp3asoc_elba_startup,
	 .hw_params = mmp3asoc_elba_hw_params,
	 },
};

/* digital audio interface glue - connects codec <--> CPU */
static struct snd_soc_dai_link mmp3_asoc_elba_dai[] = {
	{
	 .name = "ELBA I2S",
	 .stream_name = "I2S Audio",
	 .codec_name = "88pm80x-codec",
	 .platform_name = "mmp3-pcm-audio",
	 .cpu_dai_name = "mmp3-sspa-dai.0",
	 .codec_dai_name = "88pm805-i2s",
	 .ops = &mmp3asoc_machine_ops[1],
	 .init = codec_elba_init,
	 },
};

static struct snd_soc_dai_link mmp3_asoc_hdmi_dai[] = {
	{
	 .name = "HDMI",
	 .stream_name = "hdmi Audio",
	 .codec_name = "dummy-codec",
	 .platform_name = "mmp3-pcm-audio",
	 .cpu_dai_name = "mmp3-sspa-dai.0",
	 .codec_dai_name = "dummy-dai",
	 .ops = &mmp3asoc_machine_ops[0],
	 .init = codec_hdmi_init,
	 },
};

/* audio machine driver */
static struct snd_soc_card snd_soc_mmp3asoc[] = {
	{
	 .name = "mmp3 asoc",
	 .dai_link = &mmp3_asoc_elba_dai[0],
	 .num_links = 1,
	 .probe = mmp3asoc_probe,
#ifdef CONFIG_PM
	 .suspend_post = mmp3asoc_suspend_post,
	 .resume_pre = mmp3asoc_resume_pre,
#endif
	 },
	{
	 .name = "mmp3 hdmi",
	 .dai_link = &mmp3_asoc_hdmi_dai[0],
	 .num_links = 1,
	 .probe = mmp3asoc_probe,
	 },
};

static int __init mmp3asoc_init(void)
{
	int i, ret[2];

	if (!machine_is_abilene() && !machine_is_yellowstone()
		&& !machine_is_orchid() && !machine_is_thunderstonem()) {
		pr_err("%s: Unkonwn machine not supported!\n", __func__);
		return -ENODEV;
	}

	for (i = 0; i < 2; i++) {
		mmp3asoc_snd_device[i] = platform_device_alloc("soc-audio", i);
		if (!mmp3asoc_snd_device[i])
			return -ENOMEM;
		platform_set_drvdata(mmp3asoc_snd_device[i], &snd_soc_mmp3asoc[i]);
		ret[i] = platform_device_add(mmp3asoc_snd_device[i]);

		if (ret[i])
			platform_device_put(mmp3asoc_snd_device[i]);
	}

	return ret[1];
}

static void __exit mmp3asoc_exit(void)
{
	int i;
	for (i = 0; i < 2; i++)
		platform_device_unregister(mmp3asoc_snd_device[i]);
}

module_init(mmp3asoc_init);
module_exit(mmp3asoc_exit);

/* Module information */
MODULE_DESCRIPTION("ALSA SoC WM8994 MMP3");
MODULE_LICENSE("GPL");
